class SemanticModel:

    model_1_text = """[rule2: (?x manuservice:hasAccessTo ?y)
         <-(?x rdf:type manuservice:BusinessEntity), 
		 (?x manuservice:name ?n), 
		 equal(?n, 'CompanyA'),
		 (?org manuservice:hasResource ?y),
		 (?org manuservice:name ?n1),
		 equal(?n1, 'CompanyA'),
]"""
    model_2_text = """[rule3: (?x manuservice:hasAccessTo ?y)
         <-(?x rdf:type manuservice:BusinessEntity),
		(?x manuservice:hasAddress ?z),
		(?z manuservice:country ?cnt),
		equal(?cnt, 'New Zealand'),
		(?org manuservice:hasResource ?y),
		(?org manuservice:name ?n),
		equal(?n, 'CompanyB'),
]"""
    model_3_text = """[rule9: (?x manuservice:hasAccessTo ?y)
        <- (?x rdf:type manuservice:BusinessEntity), 
		(?x manuservice:creditGrade ?c), 
		ge(?c, 8.0), 
		(?org manuservice:hasResource ?y), 
		(?org manuservice:name ?n),
		equal(?n, 'CompanyD'),
]"""
    model_4_text = """[rule11: (?x manuservice:hasAccessTo ?y)
        <- (?x rdf:type manuservice:BusinessEntity),
		(?x manuservice:entityType ?t), 
		equal (?t, 'Private Limited Company'), 
		(?org manuservice:hasResource ?y), 
		(?org manuservice:name ?n), 
		equal(?n, 'CompanyF'),
]"""
    model_5_text = """[rule11: (?x manuservice:hasAccessTo ?y)
        <- (?x rdf:type manuservice:BusinessEntity),
		(?x manuservice:yearsInOperation ?o), 
		ge(?o, 10), 
		(?org manuservice:hasResource ?y), 
		(?org manuservice:name ?n), 
		equal(?n, 'CompanyF'),
		
]"""
    model_6_text = """[rule4: (?x manuservice:hasAccessTo ?y)
            <- (?x rdf:type manuservice:BusinessEntity),
    		(?y rdf:type manuservice:SoftwareResource),
    		(?org manuservice:hasResource ?y),
    		(?org manuservice:name ?n),
    		equal(?n, 'CompanyC'),
    ]"""
    model_7_text = """[rule10: (?x manuservice:hasAccessTo ?y)
        <- (?x rdf:type manuservice:BusinessEntity),
		(?y manuservice:name ?n), 
		equal(?n, 'R_NXCNC1'), 
		(?org manuservice:hasResource ?y), 
		(?org manuservice:name ?c), 
		equal(?c, 'CompanyE'),
]"""
    model_8_text = """[rule8: (?x manuservice:hasAccessTo ?y)
        <- (?x rdf:type manuservice:BusinessEntity),
		(?x manuservice:name ?z), equal (?z, 'CompanyE'), 
		(?y rdf:type manuservice:MachiningResource), 
		(?org manuservice:hasResource ?y), 
		(?org manuservice:name ?n), 
		equal(?n, 'CompanyC'),
]"""


    def __init__(self, model_id):
        self.model_id = model_id

    def get_semantic(self, nl_rule):
        word_list = nl_rule.split()
        main_company_name = word_list[0]
        if self.model_id == 1:
            return self.model_1_text.replace("CompanyA", main_company_name)
        elif self.model_id == 2:
            location = nl_rule.split(" in ")[1]
            return self.model_2_text.replace("New Zealand", location).replace("CompanyB", main_company_name)
        elif self.model_id == 3:
            rating = nl_rule.split()[len(word_list) - 1]
            return self.model_3_text.replace("CompanyD", main_company_name).replace("8.0", rating)
        elif self.model_id == 4:
            return self.model_4_text.replace("CompanyF", main_company_name)
        elif self.model_id == 5:
            operation_year = word_list[len(word_list) - 2]
            return self.model_5_text.replace("CompanyF", main_company_name).replace("10", str(round(int(operation_year))))
        elif self.model_id == 6:
            return self.model_6_text.replace("CompanyC", main_company_name)
        elif self.model_id == 7:
            return self.model_7_text.replace("CompanyE", main_company_name)
        elif self.model_id == 8:
            second_company_name = word_list[len(word_list) - 1]
            return self.model_8_text.replace("CompanyC", main_company_name).replace("CompanyE", second_company_name)










