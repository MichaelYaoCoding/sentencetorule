from keras.models import Model
from keras.layers import Dense, Input, Dropout, LSTM, Activation
from keras.layers.embeddings import Embedding
import numpy as np
import DataProcess as dp
import csv
from random import shuffle
np.random.seed(0)


def read_csv(filename='data/total_data.csv'):
    phrase = []
    rule_index = []

    with open (filename) as csvDataFile:
        csvReader = csv.reader(csvDataFile)

        for row in csvReader:
            phrase.append(row[0])
            rule_index.append(row[1])

    X = np.asarray(phrase)
    Y = np.asarray(rule_index, dtype=int)

    return X, Y


def convert_to_one_hot(Y, C):
    Y = np.eye(C)[Y.reshape(-1)]
    return Y


X_data, Y_data = read_csv('./data/total_data.csv')

index_list = [i for i in range(X_data.shape[0])]
shuffle(index_list)
X_data_shuffled = X_data[index_list]
Y_data_shuffled = Y_data[index_list]

# Prepare for training data and test data
amount_of_train_data = 128
X_train = X_data_shuffled[:amount_of_train_data]
Y_train = Y_data_shuffled[:amount_of_train_data]

X_test = X_data_shuffled[amount_of_train_data:]
Y_test = Y_data_shuffled[amount_of_train_data:]

maxLen = len(max(X_data_shuffled, key=len).split())
print(maxLen)

# convert the Y index to ont-hot format
amount_of_categories = 8
Y_oh_train = convert_to_one_hot(Y_train, C=amount_of_categories)
Y_oh_test = convert_to_one_hot(Y_test, C=amount_of_categories)


# word_to_index, index_to_word, word_to_vec_map = read_glove_vecs('./data/glove.42B.300d.txt')
word_to_index, index_to_word, word_to_vec_map = dp.read_glove_vecs('./data/glove.6B.300d.txt')

word = "cucumber"
index = 289846
print("the index of", word, "in the vocabulary is", word_to_index[word])
print("the", str(index) + "th word in the vocabulary is", index_to_word[index])


X1 = np.array(["funny lol", "lets play baseball", "food is ready for you"])
X1_indices = dp.sentences_to_indices(X1,word_to_index, max_len=5)
print("X1 =", X1)
print("X1_indices =", X1_indices)


def pretrained_embedding_layer(word_to_vec_map, word_to_index):

    vocab_len = len(word_to_index) + 1  # adding 1 to fit Keras embedding (requirement)
    emb_dim = word_to_vec_map["cucumber"].shape[0]  # define dimensionality of GloVe word vectors (= 50)

    emb_matrix = np.zeros((vocab_len, emb_dim))

    for word, index in word_to_index.items():
        emb_matrix[index, :] = word_to_vec_map[word]

    embedding_layer = Embedding(vocab_len, emb_dim, trainable=False)

    embedding_layer.build((None,))

    embedding_layer.set_weights([emb_matrix])

    return embedding_layer


embedding_layer = pretrained_embedding_layer(word_to_vec_map, word_to_index)
print("weights[0][1][3] =", embedding_layer.get_weights()[0][1][3])


def SentenceToRule(input_shape, word_to_vec_map, word_to_index):

    sentence_indices = sentence_indices = Input(input_shape, dtype='int32')

    embedding_layer = embedding_layer = pretrained_embedding_layer(word_to_vec_map, word_to_index)

    embeddings = embedding_layer(sentence_indices)

    # Propagate the embeddings through an LSTM layer with 128-dimensional hidden state
    # Be careful, the returned output should be a batch of sequences.
    X = LSTM(128, return_sequences=True)(embeddings)
    # Add dropout with a probability of 0.5
    X = Dropout(0.5)(X)
    # Propagate X trough another LSTM layer with 128-dimensional hidden state
    # Be careful, the returned output should be a single hidden state, not a batch of sequences.
    X = LSTM(128, return_sequences=False)(X)
    # Add dropout with a probability of 0.5
    X = Dropout(0.5)(X)
    # Propagate X through a Dense layer with softmax activation to get back a batch of 5-dimensional vectors.
    X = Dense(amount_of_categories)(X)
    # Add a softmax activation
    X = Activation('softmax')(X)

    # Create Model instance which converts sentence_indices into X.
    model = Model(inputs=sentence_indices, outputs=X)

    return model


model = SentenceToRule((maxLen,), word_to_vec_map, word_to_index)
model.summary()


total_accuracy = 0
loop_number = 10
for i in range(loop_number):
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    X_train_indices = dp.sentences_to_indices(X_train, word_to_index, maxLen)
    Y_train_oh = convert_to_one_hot(Y_train, C=amount_of_categories)
    model.fit(X_train_indices, Y_train_oh, epochs=50, batch_size=32, shuffle=True)

    X_test_indices = dp.sentences_to_indices(X_test, word_to_index, max_len=maxLen)
    Y_test_oh = convert_to_one_hot(Y_test, C=amount_of_categories)
    loss, acc = model.evaluate(X_test_indices, Y_test_oh)

    total_accuracy += acc
    C = amount_of_categories
    y_test_oh = np.eye(C)[Y_test.reshape(-1)]
    X_test_indices = dp.sentences_to_indices(X_test, word_to_index, maxLen)
    pred = model.predict(X_test_indices)

print("LSTM Test accuracy = ", total_accuracy / loop_number)

for i in range(len(X_test)):
    x = X_test_indices
    num = np.argmax(pred[i])
    if num != Y_test[i]:
        print('Expected category: ' + str(Y_test[i]) + '!= ' + str((num)) + ' prediction for ' + X_test[i])
    else:
        print('Expected category: ' + str(Y_test[i]) + '== ' + str((num)) + ' prediction for ' + X_test[i])