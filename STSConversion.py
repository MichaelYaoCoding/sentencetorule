from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.engine.saving import load_model
from keras.layers import Input, Embedding, LSTM, TimeDistributed, Dense, dot, Activation, concatenate
from keras.models import Model
import numpy as np
import csv
import matplotlib.pyplot as plt

from matplotlib import pyplot

from ModelTemplate import SemanticModel
from random import shuffle


np.random.seed(0)


def read_csv(filename='data/total_data_description_to_natural_language_rules.csv'):
    raw_nls = []
    nl_rules = []

    with open(filename) as csvDataFile:
        csv_reader = csv.reader(csvDataFile)

        for row in csv_reader:
            raw_nls.append(row[0])
            nl_rules.append(row[1])

    X = np.asarray(raw_nls)
    Y = np.asarray(nl_rules)
    ALL = np.asarray(raw_nls + nl_rules)

    return X, Y, ALL


print("Start to read data")

part_no = 2
file_name = './data/total_data_description_to_natural_language_rules_bigger_dataset_%s.csv' % str(part_no)

X_data, Y_data, ALL_data = read_csv(file_name)

print("Finish reading data")

index_list = [i for i in range(X_data.shape[0])]

model_size = len(X_data)

# part_range_start = (part_no - 1) * model_size
# part_range_to = part_no * model_size

part_range_start = 0
part_range_to = model_size

data_input = X_data[part_range_start: part_range_to]
data_output = Y_data[part_range_start: part_range_to]

index_list = [i for i in range(model_size)]
shuffle(index_list)
training_percent = 0.8
training_index = index_list[0: round(model_size * training_percent)]
validation_index = index_list[round(model_size * training_percent):]

training_input = data_input[training_index]
training_output = data_output[training_index]

validation_input = data_input[validation_index]
validation_output = data_output[validation_index]

# validation_input = training_input
# validatoin_output = training_output

PADDING_CHAR_CODE = 0
START_CHAR_CODE = 1


def encode_words(sentences):
    count = 2
    encoding = {}
    decoding = {1: 'START'}

    vocabulary = []
    for sentence in sentences:
        for word in sentence.split():
            if '\ufeff' in word:
                word = word.replace('\ufeff', '')
            vocabulary.append(word.lower())
    vocabulary = set(vocabulary)

    for c in vocabulary:
        encoding[c] = count
        decoding[count] = c
        count += 1
    return encoding, decoding, count


# With encoding dictionary, transform the data into a matrix
def transform(encoding, sentence_list, vector_size):
    transformed_data = np.zeros(shape=(len(sentence_list), vector_size))
    for i in range(len(sentence_list)):
        word_list = sentence_list[i].split()
        for j in range(min(len(word_list), vector_size)):
            if '\ufeff' in word_list[j]:
                word_list[j] = word_list[j].replace('\ufeff', '')
            transformed_data[i][j] = encoding[word_list[j].lower()]
    return transformed_data


# Build encoding dictionary
input_encoding, input_decoding, input_dict_size = encode_words(ALL_data)
output_encoding, output_decoding, output_dict_size = encode_words(ALL_data)

# Transform the data
vector_size = 20
encoded_training_input = transform(input_encoding, training_input, vector_size=vector_size)
encoded_training_output = transform(output_encoding, training_output, vector_size=vector_size)

# Encoder Input
training_encoder_input = encoded_training_input

# Decoder Input (need padding by START_CHAR_CODE)
training_decoder_input = np.zeros_like(encoded_training_output)
training_decoder_input[:, 1:] = encoded_training_output[:, :-1]
training_decoder_input[:, 0] = START_CHAR_CODE

training_decoder_output = np.eye(output_dict_size)[encoded_training_output.astype('int')]


# Transform for validation dataset
encoded_validation_input = transform(input_encoding, validation_input, vector_size=vector_size)
encoded_validation_ouput = transform(output_encoding, validation_output, vector_size=vector_size)

validation_encoder_input = encoded_validation_input

validation_decoder_input = np.zeros_like(encoded_validation_ouput)
validation_decoder_input[:, 1:] = encoded_validation_ouput[:, :-1]
validation_decoder_input[:, 0] = START_CHAR_CODE

# Decoder Output (one-hot encoded)
validation_decoder_output = np.eye(output_dict_size)[encoded_validation_ouput.astype('int')]


INPUT_LENGTH = vector_size
OUTPUT_LENGTH = vector_size

encoder_input = Input(shape=(INPUT_LENGTH,))
decoder_input = Input(shape=(OUTPUT_LENGTH,))

encoder = Embedding(input_dict_size, 64, input_length=INPUT_LENGTH, mask_zero=True)(encoder_input)
encoder = LSTM(64, return_sequences=True, unroll=True)(encoder)
encoder_last = encoder[:, -1, :]

decoder = Embedding(output_dict_size, 64, input_length=OUTPUT_LENGTH, mask_zero=True)(decoder_input)
decoder = LSTM(64, return_sequences=True, unroll=True)(decoder, initial_state=[encoder_last, encoder_last])

attention = dot([decoder, encoder], axes=[2, 2])
attention = Activation('softmax')(attention)

context = dot([attention, encoder], axes=[2, 1])
decoder_combined_context = concatenate([context, decoder])

# Has another weight + tanh layer as described in equation (5) of the paper
output = TimeDistributed(Dense(64, activation="tanh"))(decoder_combined_context)  # equation (5) of the paper
output = TimeDistributed(Dense(output_dict_size, activation="softmax"))(output)

model = Model(inputs=[encoder_input, decoder_input], outputs=[output])
model.compile(optimizer='adam', loss="binary_crossentropy", metrics=['accuracy'])

# mc = ModelCheckpoint('best_model.h5', monitor='val_acc', mode='max', verbose=1, save_best_only=True)
es = EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=200, min_delta=0.0005)
# mc = ModelCheckpoint('best_model.h5', monitor='val_acc', mode='max', verbose=1, save_best_only=True)

history = model.fit(x=[training_encoder_input, training_decoder_input], y=[training_decoder_output],
          validation_data=([validation_encoder_input, validation_decoder_input], validation_decoder_output),
          verbose=2,
          batch_size=64,
          epochs=20000,
          callbacks=[es])

# # load the saved model
# saved_model = load_model('best_model.h5')
# # evaluate the model
# _, train_acc = saved_model.evaluate([training_encoder_input, training_decoder_input], training_decoder_output, verbose=0)
# _, test_acc = saved_model.evaluate([validation_encoder_input, validation_decoder_input], validation_decoder_output, verbose=0)
# print('Train: %.3f, Test: %.3f' % (train_acc, test_acc))


def generate(origin_input):
    origin_input = transform(input_encoding, [origin_input.lower()], INPUT_LENGTH)
    destination_input = np.zeros(shape=(len(origin_input), OUTPUT_LENGTH))
    destination_input[:, 0] = START_CHAR_CODE
    for i in range(1, OUTPUT_LENGTH):
        output = model.predict([origin_input, destination_input]).argmax(axis=2)
        destination_input[:, i] = output[:, i]
    return destination_input[:, 1:]


def decode(sequence, original_word, second_original_word):
    final_translation = ''
    for i in sequence:
        if i == 0:
            break
        final_translation += output_decoding[i]
        final_translation += ' '

    # if len(original_word) > 0:
    #     final_translation = final_translation.replace('google', original_word)
    # if len(second_original_word) > 0:
    #     final_translation = final_translation.replace('ibm', second_original_word)
    return final_translation


def translate(origin):
    origin = origin.lower()
    original_word = ''
    second_original_word = ''
    word_list = origin.split()
    replace_count = 0
    for i in range(len(word_list)):
        word = word_list[i]
        # if word not in input_encoding.keys() and replace_count == 0:
        #     original_word = word
        #     origin = origin.replace(original_word, 'google')
        #     replace_count = 1
        # elif word not in input_encoding.keys() and replace_count == 1:
        #     second_original_word = word
        #     origin = origin.replace(second_original_word, 'ibm')
        #     replace_count = 2

    decoder_output = generate(origin)
    return decode(decoder_output[0], original_word, second_original_word)


def validate():
    count = 0
    for i in range(len(validation_output)):
        word_in_str1 = validation_output[i].lower().split()
        word_in_str_2 = translate(validation_input[i]).lower().split()
        trimed_word_in_str1 = []
        trimed_word_in_str2 = []
        for word in word_in_str1:
            trimed_word_in_str1.append(word.strip())

        for word in word_in_str_2:
            if '\ufeff' in word:
                word = word.replace('\ufeff', '')
            trimed_word_in_str2.append(word.strip())

        str_1 = " ".join(trimed_word_in_str1)
        str_2 = " ".join(trimed_word_in_str2)
        if str_1 not in str_2:
            count += 1
            print(str(i) + ": ")
            print('X:         ', validation_input[i])
            print('Y:         ', str_1)
            print('Predicted: ', str_2)
            print(' ')

        if i == len(validation_output) - 1:
            semantic_model = SemanticModel(part_no)
            print(semantic_model.get_semantic(str_2))

    print('Part number: ', part_no)
    print('Error rate: ', count / len(validation_output))


validate()

# plot training history
# pyplot.plot(history.history['loss'], label='train')
# pyplot.plot(history.history['val_loss'], label='test')
# pyplot.plot(history.history['acc'], label='train')
# pyplot.plot(history.history['val_acc'], label='test')
# pyplot.legend()
# pyplot.show()
print(history.history.keys())
# summarize history for accuracy
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()


# print(translate('Wechat assigns machining resources with Tecent'))








