import numpy as np
import DataProcess as dp
import csv
from random import shuffle
np.random.seed(0)
from keras.models import Model
from keras.layers import Dense, Input, Dropout, LSTM, Activation
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
from keras.initializers import glorot_uniform
import DataProcess


def read_csv(filename='data/total_data_description_to_natural_language_rules.csv'):
    raw_nls = []
    nl_rules = []

    with open(filename) as csvDataFile:
        csv_reader = csv.reader(csvDataFile)

        for row in csv_reader:
            raw_nls.append(row[0])
            nl_rules.append(row[1])

    X = np.asarray(raw_nls)
    Y = np.asarray(nl_rules)

    return X, Y


print("Start to read data")

X_data, Y_data = read_csv('./data/total_data_description_to_natural_language_rules.csv')

print("Finish reading data")

index_list = [i for i in range(X_data.shape[0])]

model_size = 20

X_data_first = X_data[:model_size]
Y_data_first = Y_data[:model_size]

maxLen = len(max(X_data_first, key=len).split())
print("Max length of X:", maxLen)

word_to_index, index_to_word, word_to_vec_map = dp.read_glove_vecs('./data/glove.6B.300d.txt')


def pretrained_embedding_layer(word_to_vec_map, word_to_index):

    vocab_len = len(word_to_index) + 1  # adding 1 to fit Keras embedding (requirement)
    emb_dim = word_to_vec_map["cucumber"].shape[0]  # define dimensionality of GloVe word vectors (= 50)

    emb_matrix = np.zeros((vocab_len, emb_dim))

    for word, index in word_to_index.items():
        emb_matrix[index, :] = word_to_vec_map[word]

    embedding_layer = Embedding(vocab_len, emb_dim, trainable=False)

    embedding_layer.build((None,))

    embedding_layer.set_weights([emb_matrix])

    return embedding_layer


embedding_layer = pretrained_embedding_layer(word_to_vec_map, word_to_index)


def SentenceToRule(input_shape, word_to_vec_map, word_to_index):

    sentence_indices = sentence_indices = Input(input_shape, dtype='int32')

    embedding_layer = embedding_layer = pretrained_embedding_layer(word_to_vec_map, word_to_index)

    embeddings = embedding_layer(sentence_indices)

    # Propagate the embeddings through an LSTM layer with 128-dimensional hidden state
    # Be careful, the returned output should be a batch of sequences.
    X = LSTM(128, return_sequences=True)(embeddings)
    # Add dropout with a probability of 0.5
    X = Dropout(0.5)(X)
    # Propagate X trough another LSTM layer with 128-dimensional hidden state
    # Be careful, the returned output should be a single hidden state, not a batch of sequences.
    X = LSTM(128, return_sequences=False)(X)
    # Add dropout with a probability of 0.5
    X = Dropout(0.5)(X)
    # Propagate X through a Dense layer with softmax activation to get back a batch of 5-dimensional vectors.
    X = Dense(amount_of_categories)(X)
    # Add a softmax activation
    X = Activation('softmax')(X)

    # Create Model instance which converts sentence_indices into X.
    model = Model(inputs=sentence_indices, outputs=X)

    return model


model = SentenceToRule((maxLen,), word_to_vec_map, word_to_index)
model.summary()












